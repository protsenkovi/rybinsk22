import os
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"

import numpy as np
import cv2
from glob import glob
from tqdm import tqdm
import tensorflow as tf
from tensorflow.keras.utils import CustomObjectScope
from metrics import dice_loss
from metrics import dice_coef
from metrics import iou
from data import line_contrasting
from conf import *

def find_edges(img):
    mn = img.min()
    mx = img.max()
    return (mn, mx)

def line_contrasting(img):
    copy = np.array(img, copy=True)
    min, max = find_edges(img)
    copy = (img-min)/(max-min)*255
    return copy.astype('uint8')

def create_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def prepare_test():
    path = "test"
    np.random.seed(42)
    X = sorted(glob(os.path.join(path, "*.jpg"))) + sorted(glob(os.path.join(path, "*.bmp")))
    create_dir(f"new_data/test")

    for x in X:
        name = x.split("tests")[-1].split(".")[0]
        x = cv2.imread(x, cv2.IMREAD_COLOR)
        X = [x]
        for i in X:
            i = cv2.resize(i, (W, H))
            i = line_contrasting(i)

        image_path = os.path.join(f"new_data", name)
        print(image_path)
        cv2.imwrite(f"{image_path}.png", i)

def predict_val(label):
    np.random.seed(42)
    tf.random.set_seed(42)
    create_dir(f"results/validate/{label}")

    with CustomObjectScope({'iou': iou, 'dice_coef': dice_coef, 'dice_loss': dice_loss}):
        model = tf.keras.models.load_model(f"files/model_{label}.h5")

    path_test = "new_data/validate"

    test_x = sorted(glob(os.path.join(path_test, "*.png")))

    print(len(test_x))

    for x in test_x:
        name = x.split("validate")[-1].split(".")[0]
        name = name[1:]
        img = cv2.imread(x, cv2.IMREAD_COLOR)
        x = line_contrasting(img)
        x = x/255.0
        x = np.expand_dims(x, axis=0)

        p = model.predict(x)[0]
        p = np.squeeze(p, axis=-1)
        p = p > 0.5
        p = p.astype(np.int32)

        save_img_path = f"results/validate/{label}/{name}.png"
        print(save_img_path)
        cv2.imwrite(save_img_path, p)

def save_masks(label):
    np.random.seed(42)
    tf.random.set_seed(42)
    create_dir(f"results/{label}/test_masks")

    with CustomObjectScope({'iou': iou, 'dice_coef': dice_coef, 'dice_loss': dice_loss}):
        model = tf.keras.models.load_model(f"files/model_{label}.h5")

    path_test = f"new_data/test"

    test_x = sorted(glob(os.path.join(path_test, "*.png")))

    print(len(test_x))

    for x in test_x:
        name = x.split("test")[-1].split(".")[0]
        name = name[1:]

        img = cv2.imread(x, cv2.IMREAD_COLOR)
        x = line_contrasting(img)
        x = x/255.0
        x = np.expand_dims(x, axis=0)

        p = model.predict(x)[0]
        p = np.squeeze(p, axis=-1)
        p = p > 0.5
        p = p.astype(np.int32)

        save_img_path = f"results/{label}/test_masks/{name}.png"
        cv2.imwrite(save_img_path, p)

def prepare_val():
    create_dir("new_data/validate")
    path = "validate/imgs"
    np.random.seed(42)
    X = sorted(glob(os.path.join(path, "*.jpg")))
    for x in X:
        name = x.split("imgs")[-1].split(".")[0]
        name = name[1:]
        x = cv2.imread(x, cv2.IMREAD_COLOR)
        X = [x]
        for i in X:
            i = cv2.resize(i, (W, H))
            i = line_contrasting(i)

        image_path = os.path.join("new_data","validate", name)
        print(image_path)
        cv2.imwrite(f"{image_path}.png", i)
