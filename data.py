import os
import numpy as np
import cv2
from glob import glob
from tqdm import tqdm
from conf import *

def create_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def load_data(path):
    X = sorted(glob(os.path.join(path, "imgs", "*.jpg")))
    Y = sorted(glob(os.path.join(path, "masks", "*.png")))
    return X, Y

def find_edges(img):
    mn = img.min()
    mx = img.max()
    return (mn, mx)

def line_contrasting(img):
    copy = np.array(img, copy=True)
    min, max = find_edges(img)
    copy = (img-min)/(max-min)*255
    return copy.astype('uint8')

def augment_data(images, masks, save_path, label):
    for x, y in tqdm(zip(images, masks), total=len(images)):
        name = x.split("imgs")[-1].split(".")[0]
        name = name[1:]

        x = cv2.imread(x, cv2.IMREAD_COLOR)
        y = cv2.imread(y, cv2.IMREAD_GRAYSCALE)

        X = [x]
        Y = [y]

        for i, m in zip(X, Y):
            i = cv2.resize(i, (W, H))
            m = cv2.resize(m, (W, H))
            i = line_contrasting(i)

            if label == 1:
                tmp_image_name = f"{name}.png"
                tmp_mask_name = f"{name}.png"

                for ii in range(len(m)):
                    for jj in range(len(m[0])):
                        if m[ii][jj] == 2: m[ii][jj] = 1
                        if m[ii][jj] == 3: m[ii][jj] = 1
                        if m[ii][jj] == 4: m[ii][jj] = 1
                        if m[ii][jj] == 5: m[ii][jj] = 1
                        if m[ii][jj] == 6: m[ii][jj] = 1

                image_path = os.path.join(save_path, "imgs", tmp_image_name)
                mask_path = os.path.join(save_path, "masks", tmp_mask_name)

                cv2.imwrite(image_path, i)
                cv2.imwrite(mask_path, m)
                
            else:
                tmp_image_name = f"{name}.png"
                tmp_mask_name = f"{name}.png"
                for ii in range(len(m)):
                    for jj in range(len(m[0])):
                        if m[ii][jj] == 1: m[ii][jj] = 0
                        if m[ii][jj] == label: m[ii][jj] = 1

                image_path = os.path.join(save_path, "imgs", tmp_image_name)
                mask_path = os.path.join(save_path, "masks", tmp_mask_name)

                i = line_contrasting(i)

                cv2.imwrite(image_path, i)
                cv2.imwrite(mask_path, m) 


def prepare_data(label):
    np.random.seed(42)

    data_path = "data"
    train_x, train_y = load_data(data_path)

    print(f"Train:\t {len(train_x)} - {len(train_y)}")

    create_dir(f"new_data/{label}/imgs/")
    create_dir(f"new_data/{label}/masks/")

    augment_data(train_x, train_y, f"new_data/{label}/", label)
