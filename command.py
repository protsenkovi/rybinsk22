import os
from glob import glob
from data import prepare_data
from model import check_model
from train import train
from res import prepare_test
from res import save_masks
from unite import unite
from res import prepare_val
from res import predict_val
from unite import unite_val



os.environ["CUDA_VISIBLE_DEVICES"] = "0"

commands = ["- check",
     "- prepare",
     "- prepare_in_range",
     "- check_model",
     "- train",
     "- train_in_range",
     "- prepare_test",
     "- test",
     "- test_in_range",
     "- unite",  
     "- help"]

def help():
    print("Possible commands:")
    for command in commands:
        print(command)

help()
command = ""
while command != 'exit':
    command = input(':::')
    if command == "check":
        i = input("choose label:::")
        X = sorted(glob(os.path.join(f"new_data/{i}", "imgs", "*.png")))
        Y = sorted(glob(os.path.join(f"new_data/{i}", "masks", "*.png")))
        print(len(X))
        print(len(Y))

    if command == "prepare":
        i = input("choose label (1-6):::")
        prepare_data(i)

    if command == "prepare_in_range":
        start = input("choose start label:::")
        end = input("choose last label:::")
        for i in range(int(start), int(end)+1):
            prepare_data(i)
    
    if command == "check_model":
        q = input("Stardatr?(y or n):::")
        if q == "y":
            check_model("sigmoid", "sigmoid", 6, 12, 18)
        if q == "n":
            act1 = input("choose act1(Standart: sigmoid):::")
            act2 = input("choose act2(Standart: sigmoid):::")
            dr1 = input("choose dilation rate 1(Standart: 6):::")
            dr2 = input("choose dilation rate 2(Standart: 12):::")
            dr3 = input("choose dilation rate 3(Standart: 18):::")
            check_model(act1, act2, dr1, dr2, dr3)

    if command == "train":
        i = input("choose label")
        q = input("Stardatr?(y or n):::")
        if q == "y":
            train(int(i), "sigmoid", "sigmoid", 6, 12, 18)
        if q == "n":
            act1 = input("choose act1(Standart: sigmoid):::")
            act2 = input("choose act2(Standart: sigmoid):::")
            dr1 = input("choose dilation rate 1(Standart: 6):::")
            dr2 = input("choose dilation rate 2(Standart: 12):::")
            dr3 = input("choose dilation rate 3(Standart: 18):::")
            train(int(i), act1, act2, dr1, dr2, dr3)

    if command == "train_in_range":
        start = input("choose start:::")
        end = input("choose end:::")
        for i in range(int(start), int(end)+1):
            train(i, "sigmoid", "sigmoid", 6, 12, 18)

    if command == "prepare_test":
        prepare_test()

    if command == "test":
        i = input ("choose label:::")
        save_masks(int(i))

    if command == "test_in_range":
        start = input("choose start label:::")
        end = input("choose last label:::")
        for i in range(int(start), int(end)+1):
            save_masks(i)

    if command == "unite":
        unite()

    if command == "prepare_val":
        prepare_val()

    if command == "predict_val":
        i = input("choose label:::")
        predict_val(int(i))

    if command == "valge":
        start = input("choose start label:::")
        end = input("choose last label:::")
        for i in range(int(start), int(end)+1):
            predict_val(i)


    if command == "acc":
        unite_val()

    if command == "help":
        help()
