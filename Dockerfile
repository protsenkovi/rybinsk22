FROM tensorflow/tensorflow:2.9.0rc2-gpu
RUN apt update && apt-get install libgl1 -y
RUN pip install opencv-python 
RUN pip install sklearn 
RUN pip install tqdm
RUN pip install dvc[webdav]
RUN mkdir /workdir
WORKDIR /workdir
CMD ["python", "command.py"]
