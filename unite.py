import cv2
from glob import glob
import os 
from data import create_dir
from sklearn.metrics import accuracy_score

create_dir("final_result")
def unite():
    path = 'results/1'
    Y = sorted(glob(os.path.join(path, "test_masks", "*.png")))
    for y in Y:
        name = y.split("masks")[-1].split(".")[0]
        name = name[1:]
        mask = cv2.imread(f"{path}/test_masks/{name}.png", cv2.IMREAD_GRAYSCALE)
        for label in range(2,7):
            tmp = cv2.imread(f"results/{label}/test_masks/{name}.png", cv2.IMREAD_GRAYSCALE)
            height, width = tmp.shape
            for i in range(height):
                for j in range(width):
                    if mask[i][j] == 1:
                        if tmp[i][j] == 1:
                            mask[i][j] = label

        mask = cv2.resize(mask, (5496, 3672))
        mask = mask.astype("uint8")
        print(name)
        cv2.imwrite(f"final_result/{name}_seg.png", mask)

def unite_val():
    path = 'results/validate'
    create_dir("final_validate")
    Y = sorted(glob(os.path.join(path, "1", "*.png")))
    total = 0
    for y in Y:
        name = y.split("validate")[-1]
        name = name[3:]
        print(name)
        mask = cv2.imread(f"{path}/{1}/{name}", cv2.IMREAD_GRAYSCALE)
        for label in range(2,7):
            tmp = cv2.imread(f"results/validate/{label}/{name}", cv2.IMREAD_GRAYSCALE)
            height, width, channels = tmp.shape
            for i in range(height):
                for j in range(width):
                    if mask[i][j] == 1:
                        if tmp[i][j] == 1:
                            mask[i][j] = label

        mask = list(cv2.resize(mask, (5496, 3672)))
        orig = list(cv2.imread(f"validate/masks/{name}", cv2.IMREAD_GRAYSCALE))
        a_m = []
        a_o = []
        for m in mask:
            for mm in m:
                a_m.append(mm)
        for o in orig:
            for oo in o:
                a_o.append(oo)
        a = accuracy_score(a_o, a_m)
        print(a)
        total+=a
    print(f"total: {total/len(Y)}")